#! /usr/bin/env hoomd
from __future__ import print_function, division
from hoomd import *
from hoomd import md
from hoomd import deprecated
import sys
import numpy
import math
import time

c = context.initialize()

prefix='BB20_SC2_10_SC10_10_run1'


system=init.read_gsd(filename=prefix+'.gsd')
nlpoly = md.nlist.cell(r_buff=0.5);
nlpoly.reset_exclusions(exclusions = ['bond','constraint'])

groupA=group.type(name='BB',type='A')
groupB=group.type(name='grafted',type='B')
groupC=group.type(name='solvent',type='C')
groupAB=group.union(name='BBall',a=groupA,b=groupB)

harmonic = md.bond.harmonic(name="mybond")
harmonic.bond_coeff.set('polymer', k=100, r0=1)


lj = md.pair.lj(r_cut=3, nlist=nlpoly)
lj.pair_coeff.set(['A','B'], ['A','B'], sigma=1.0, epsilon=0.5)
lj.pair_coeff.set(['A','B','C'], ['C'], sigma=1.0, epsilon=1.0)


#warmup output
#GSDwarmup=dump.gsd(filename=prefix+"_warmup.gsd", period=1000, group=group.all(), phase=0,overwrite=True)
#logwarmup=analyze.log(filename=prefix+"_warmup.log",
#                  quantities=['potential_energy', 'temperature', 'pair_lj_energy','bond_harmonic_energy'],
#                  period=100,
#                  overwrite=True);


#startmin
fire=md.integrate.mode_minimize_fire(dt=0.01, ftol=1e-2, Etol=1e-7)
nve=md.integrate.nve(group.all(),limit=0.01)

run(1000)

nve.disable()
del nve
del fire

#first integration and replication
integrate=md.integrate.mode_standard(dt=0.001)
nvt=md.integrate.nvt(group=group.all(), tau=0.1, kT=1.0)

run(10000)

#integrate.set_params(dt=0.01)
#run(1e4)

#box_resize = update.box_resize(Lz = variant.linear_interp([(0,9),(1e4,70)]),Ly=70,Lx=70)
#run(1e4)

#box_resize = update.box_resize(L = variant.linear_interp([(0,70),(1e4,20)]))
#run(1e4)

nvt.disable()
del nvt
npt=md.integrate.npt(group=group.all(), tau=0.1, kT=1.0,tauP=1,P=0.082) #1 atm, 
run(1e4)

npt.set_params(tauP=0.1)
run(1e5)

npt.disable()
#system.replicate(nx=2,ny=2,nz=2)
del npt
integrate.set_params(dt=0.001)
nvt=md.integrate.nvt(group=group.all(), tau=0.1, kT=1.0)
nvt.randomize_velocities(seed=int(time.time()))
zeroer= md.update.zero_momentum(period=10)
run(1e4)
zeroer.disable()

nvt.disable()
del nvt

#warmupRun
integrate.set_params(dt=0.01)
npt=md.integrate.npt(group=group.all(), tau=0.1, kT=1.0,tauP=0.1,P=0.082) 
run(1e5)

#Production Run
logw=analyze.log(filename=prefix+"_traj.log",
                  quantities=['potential_energy', 'temperature', 'pair_lj_energy','bond_harmonic_energy','volume'],
                  period=100,
                  overwrite=True);
GSD=dump.gsd(filename=prefix+"_traj.gsd", period=100000, group=group.all(), phase=0,overwrite=True)
GSDBB=dump.gsd(filename=prefix+"_BBtraj.gsd", period=10000, group=groupAB, phase=0,overwrite=True)
run(1e8)
