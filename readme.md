# MATLAB gsd reader
Andrew Peters   [Home Page](http://www2.latech.edu/~apeters/index.html)  [GitLab Page](https://gitlab.com/users/Andrew-Peters/contributed)

## Overview

Included are the necessary files to read and write the GSD (General Simulation Data) file format. This format is used by [hoomd](https://glotzerlab.engin.umich.edu/hoomd-blue/) and is maintained and created by the glotzer group. 

##Installation

Install via:
```bash
$ git clone https://gitlab.com/MATLAB-HOOMD-System/matlab_gsd_reader.git
```
Then copy .m files and test files (if desired) to MATLAB path.


##Dependencies

The glotzer group gsd code is required. This code interfaces MATLAB with the gsd python code to read files into a structure for use in matlab and then write from that structure to a gsd file. This can be installed from https://bitbucket.org/glotzer/gsd/src/master/ or by using conda. The required environment is included and can be constructed using:

```bash
$ conda env create --name MATLAB --file MATLAB_env.txt
```
To run MATLAB, first change to this environment, then run MATLAB.

```bash
$ conda activate MATLAB
$ cd ~/MATLAB  #I advise my students to always run from with a MATLAB folder that contains all of their code in some subdirectory. This is not strictly necessary.
$ matlab
```

##Examples

Included is an example gsd file ('example.gsd') that can be read. 

Read the example gsd file.
```matlab
>>gsdname='path/to/example/file/example.gsd';
>>[system] = Extract_GSD(gsdname,'AllFlag',1,'Steps',[0 50])
```

This will read in the example file, read all components, and the read steps 0 to 50. 

Write out the read system file
```matlab
>>gsdnameread='path/to/example/file/example.gsd';
>>[system] = Extract_GSD(gsdnameread,'AllFlag',1,'Steps',[0 50])
>>gsdnamewrite='path/to/example/file/testwrite.gsd';
>>Write_GSD(system,gsdnamewrite)
```

This will write out the system file loaded in the previous step to testwrite.gsd. To not save all steps or data from system, remove that data from system. 

