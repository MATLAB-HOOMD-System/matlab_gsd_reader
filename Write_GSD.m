function Write_GSD(system,gsdname)%unwrappedflag)

%Write_GSD(system,gsdname)
%
%Writes a GSD file from system using the glotzer group python code: https://bitbucket.org/glotzer/gsd/src/master/
%system is a structure with various fields defining the system and its
%trajectory. At the moment, not change in topology is possible, though the
%gsd format supports such changes. 
%
%Required system fields include:
%natoms:number of atoms
%attype: atom types defined by a single character (A, B, C, D, etc.)
%dim: box dimensions. A 1x3 array defining the x y and  dimensions of a rectangular box.
%pos: particle positions. Nx3xT where N is the number of particles, and T is the number of timesteps.
%
%
%Optional fields include 
%bonds: Nx2 array specifying a bond between two particles. Numbering
%startsfrom 0. Default Bond name is polymer. Can be optionally specified as
%system.bondname. Only one type of bond is currently allowed. 
%angles:Nx3 array specifying a bond between two particles. Numbering start
%from 0. Default Bond name is polymer. Can be optionally specified as
%system.anglename. Only one type of angle is currently allowed. 
%mass: Will not write. Default mass is 1
%diameter: default diameter is 1.
%body: defauly is -1
%stepsize: defaults to 1 if writing more than one timestep
%initial timestep: defaults to 0 
%vel: velocity. Nx3xT
%img: image. Nx3xT
%moment_inertia: moment of inertia. Nx3xT.
%orientation: Orientation of Rigid Bodies. Nx4xT

py.sys.setdlopenflags(int32(10));

if ~exist('gsdname','var')
    gsdname='test.gsd';
end


%natoms and atom types
s=py.gsd.hoomd.Snapshot()
if ~isfield(system,'natoms')
    system.natoms=length(system.attype)
elseif system.natoms~= length(system.attype)
    system.natoms=length(system.attype)
end
    
%%%checkattype size
if system.natoms~=size(system.attype,1)
    system.attype=system.attype';
end

s.particles.N=int32(system.natoms)
types=unique(system.attype,'rows');
for i=1:size(types,1)
%     types2{i}=types(i);
    types2{i,:}=types(i,:);
end
s.particles.types=types2';


%Check that attype is a column vector
if size(system.attype,1)==1 && size(system.attype,2)>1
    system.attype=system.attype';
end
Types=unique(system.attype,'rows');
for i=1:system.natoms
%     typeid(i)=find(Types==system.attype(i,:),1)-1;
    typeid(i)=find(ismember(Types,system.attype(i,:),'rows'))-1;
end
s.particles.typeid=typeid

%body
clear typeid
if isfield(system,'body')
        s.particles.body=system.body;
end



%bonds
clear typeid
if isfield(system,'bonds')
    tempbonds=reshape(permute(system.bonds,[2 1]),1,size(system.bonds,1)*2);
    tempbonds=py.numpy.array(int32(tempbonds));
    tempbonds=py.numpy.reshape(tempbonds,[size(system.bonds,1),int32(2)]);
    s.bonds.group=tempbonds;
    s.bonds.N=int32(size(system.bonds,1));
    if isfield(system,'bondnames')
        bondtypes=unique(system.bondnames,'rows');
        for i=1:size(bondtypes,1)
        %     types2{i}=types(i);
            bondtypes2{i,:}=bondtypes(i,:);
        end
        s.bonds.types=bondtypes2';
        for i=1:size(system.bonds,1)
            typeid(i)=find(ismember(bondtypes,system.bondnames(i,:),'rows'))-1;
        end
        s.bonds.typeid=py.numpy.array(int32(typeid));
    else
        s.bonds.types={'polymer'};
    end
end


% s.bonds.N=2;
% s.bonds.types=['A','B'];
% s.bonds.typeid=[0 1];
% s.bonds.group=[0 1; 1 2];

% if isfield(system,'bondtypes')
    

%angles
if isfield(system,'angles')
    clear typeid
    tempangles = reshape(permute(system.angles,[3 2 1]),1,size(system.angles,1)*3);
    tempangles=py.numpy.array(tempangles);
    tempangles=py.numpy.reshape(tempangles,[size(system.angles,1),int32(3)]);
    s.angles.group=tempangles;
    s.angles.N=int32(size(system.angles,1))
    
    if isfield(system,'anglenames')
        angletypes=unique(system.anglenames,'rows');
        for i=1:size(angletypes,1)
        %     types2{i}=types(i);
            angletypes2{i,:}=angletypes(i,:);
        end
        s.angles.types=angletypes2';
        for i=1:size(system.angles,1)
            typeid(i)=find(ismember(angletypes,system.anglenames(i,:),'rows'))-1;
        end
        s.angles.typeid=py.numpy.array(int32(typeid));
    else
        s.angles.types={'polymer'};
    end
end

%constraints (distance)
if isfield(system,'constraints')
    clear tempbonds
    tempbonds=reshape(permute(system.constraints,[2 1]),1,size(system.constraints,1)*2);
    tempbonds=py.numpy.array(int32(tempbonds));
    tempbonds=py.numpy.reshape(tempbonds,[size(system.constraints,1),int32(2)]);
    s.constraints.group=tempbonds;
    s.constraints.N=int32(size(system.constraints,1));
    s.constraints.value=system.constraintvalues';
end




%dihedrals
clear typeid
if isfield(system,'dihedrals')
    tempdihedrals = reshape(permute(system.dihedrals,[4 3 2 1]),1,size(system.dihedrals,1)*4);
    tempdihedrals=py.numpy.array(tempdihedrals);
    tempdihedrals=py.numpy.reshape(tempdihedrals,[size(system.dihedrals,1),int32(4)]);
    s.dihedrals.group=tempdihedrals;
    s.dihedrals.N=int32(size(system.dihedrals,1))
    
    if isfield(system,'dihedralnames')
        dihedraltypes=unique(system.dihedralnames,'rows');
        for i=1:size(dihedraltypes,1)
        %     types2{i}=types(i);
            dihedraltypes2{i,:}=dihedraltypes(i,:);
        end
        s.dihedrals.types=dihedraltypes2';
        for i=1:size(system.dihedrals,1)
            typeid(i)=find(ismember(dihedraltypes,system.dihedralnames(i,:),'rows'))-1;
        end
        s.dihedrals.typeid=py.numpy.array(int32(typeid));
    else
        s.dihedrals.types={'polymer'};
    end
end

%special pairs
clear typeid
if isfield(system,'pairs')
    temppairs=reshape(permute(system.pairs,[2 1]),1,size(system.pairs,1)*2);
    temppairs=py.numpy.array(int32(temppairs));
    temppairs=py.numpy.reshape(temppairs,[size(system.pairs,1),int32(2)]);
    s.pairs.group=temppairs;
    s.pairs.N=int32(size(system.pairs,1));
    if isfield(system,'pairnames')
        pairtypes=unique(system.pairnames,'rows');
        for i=1:size(pairtypes,1)
        %     types2{i}=types(i);
            pairtypes2{i,:}=pairtypes(i,:);
        end
        s.pairs.types=pairtypes2';
        for i=1:length(system.pairs)
            typeid(i)=find(ismember(pairtypes,system.pairnames(i,:),'rows'))-1;
        end
        s.pairs.typeid=py.numpy.array(int32(typeid));
    else
        s.pairs.types={'polymer'};
    end
end

%impropers
clear typeid
if isfield(system,'impropers')
    tempimpropers = reshape(permute(system.impropers,[4 3 2 1]),1,size(system.impropers,1)*4);
    tempimpropers=py.numpy.array(tempimpropers);
    tempimpropers=py.numpy.reshape(tempimpropers,[size(system.impropers,1),int32(4)]);
    s.impropers.group=tempimpropers;
    s.impropers.N=int32(size(system.impropers,1))
    
    if isfield(system,'impropernames')
        impropertypes=unique(system.impropernames,'rows');
        for i=1:size(impropertypes,1)
        %     types2{i}=types(i);
            impropertypes2{i,:}=impropertypes(i,:);
        end
        s.impropers.types=impropertypes2';
        for i=1:length(system.impropers)
            typeid(i)=find(ismember(impropertypes,system.impropernames(i,:),'rows'))-1;
        end
        s.impropers.typeid=py.numpy.array(int32(typeid));
    else
        s.impropers.types={'polymer'};
    end
end

%diameter
if isfield(system,'diameter')
    s.particles.diameter=system.diameter'
end

%charge
if isfield(system,'charge')
    if size(system.charge,1)>size(system.charge,2)
        s.particles.charge=system.charge'
    else
        s.particles.charge=system.charge;
    end
end


%timestep
if isfield(system,'timestep')
    s.configuration.step=int32(system.timestep)
elseif isfield(system,'initialstep')
    if size(system.pos,3)==1
        s.configuration.step=int32(system.initialstep)
    else
        s.configuration.step=int32(system.initialstep+system.stepsize*(size(system.pos,3)-1))
    end
else
    system.timestep=0;
    s.configuration.step=int32(system.timestep)
end

%mass
if ~isfield(system,'mass')
    system.mass=ones(size(system.attype));
end
if size(system.mass,1)>size(system.mass,2)
    s.particles.mass=system.mass'
end

%dim
if numel(system.dim)==3
    s.configuration.box = [system.dim, 0, 0, 0]
    if isfield(system,'dimensions')
        s.configuration.dimensions=system.dimensions
    end
end
%open for writing
t=py.gsd.hoomd.open(gsdname,'wb')

%write dynamics
for i=1:size(system.pos,3)
    if i==1
        if isfield(system,'initialstep')
            s.configuration.step=system.initialstep;
        else
            s.configuration.step=system.timestep
        end;
    else
        s.configuration.step=system.initialstep+system.stepsize*(i-1);
    end
    % dynamic box size
    if numel(system.dim)~=3
        s.configuration.box = [system.dim(i,:), 0, 0, 0]
    end


    %position
    temppos=system.pos(:,:,i);
    temppos2=temppos;
    temppos = reshape(permute(temppos,[3 2 1]),1,system.natoms*3);
    temppos=py.numpy.array(temppos);
    temppos=py.numpy.reshape(temppos,[system.natoms,int32(3)]);
    s.particles.position=temppos;
    
    %velocity
    if isfield(system,'vel')
        tempvel=system.vel(:,:,i);
        tempvel = reshape(permute(tempvel,[3 2 1]),1,system.natoms*3);
        tempvel=py.numpy.array(tempvel);
        tempvel=py.numpy.reshape(tempvel,[system.natoms,int32(3)]);
        s.particles.velocity=tempvel;
    end
    
    %image
    if isfield(system,'img')
        tempimg=system.img(:,:,i);
        tempimg = reshape(permute(tempimg,[3 2 1]),1,system.natoms*3);
        tempimg=py.numpy.array(int32(tempimg));
        tempimg=py.numpy.reshape(tempimg,[system.natoms,int32(3)]);
        s.particles.image=tempimg;
    end
    
    %moment_inertia
    if isfield(system,'moment_inertia')
        tempmoment_inertia=system.moment_inertia(:,:,i);
        tempmoment_inertia = reshape(permute(tempmoment_inertia,[3 2 1]),1,system.natoms*3);
        tempmoment_inertia=py.numpy.array(tempmoment_inertia);
        tempmoment_inertia=py.numpy.reshape(tempmoment_inertia,[system.natoms,int32(3)]);
        s.particles.moment_inertia=tempmoment_inertia;
    end
    
    %orientation
    if isfield(system,'orientation')
        temporientation=system.orientation(:,:,i);
        temporientation = reshape(permute(temporientation,[4 3 2 1]),1,system.natoms*4);
        temporientation=py.numpy.array(temporientation);
        temporientation=py.numpy.reshape(temporientation,[system.natoms,int32(4)]);
        s.particles.orientation=temporientation;
    end
    
    
    
    
    
    %progress tracking
    %disp(['step #' num2str(s.configuration.step)])
    
    %append step
    t.append(s);
end
